package com.piggymetrics.gateway;


import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;


/**
 * 添加服务过滤，并不是所有的请求都可以进来，都可以经过我的路由的
 */
public class AccessFilter extends ZuulFilter {
    private static Logger log = LoggerFactory.getLogger(AccessFilter.class);
    //必须实现这几个方法。
    @Override
    public String filterType() {
        // filterType：返回一个字符串代表过滤器的类型，在zuul中定义了四种不同生命周期的过滤器类型，具体如下：
        // pre：可以在请求被路由之前调用
        // routing：在路由请求时候被调用
        // post：在routing和error过滤器之后被调用
        //error：处理请求时发生错误时被调用
        return "pre";
    }
    @Override
    public int filterOrder() {

        //filterOrder：通过int值来定义过滤器的执行顺序

        return 0;
    }
    @Override
    public boolean shouldFilter() {
        return true;
    }
    @Override
    public Object run() {
    //run：过滤器的具体逻辑。需要注意，这里我们通过ctx.setSendZuulResponse(false)令zuul过滤该请求，不对其进行路由，
    // 然后通过ctx.setResponseStatusCode(401)设置了其返回的错误码，当然我们也可以进一步优化我们的返回，
    // 比如，通过ctx.setResponseBody(body)对返回body内容进行编辑等。
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL().toString()));
        Object accessToken = request.getParameter("accessToken");
        //下面就过滤了
        if(accessToken == null) {
            //如果获取不到这个token，就不认这个应用，
            log.warn("access token 是空的");
            ctx.setSendZuulResponse(false);//过滤这个请求
            ctx.setResponseStatusCode(401);//返回码
            return null;
        }
        log.info("正确的token，可以进入");
        return null;
    }
}