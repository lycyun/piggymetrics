package com.piggymetrics.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

/**
 *
 * zuul的注释以及client
 * 配置依赖configs erver
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}
//	//添加了这句话就能实现自定义的过滤了，先注释，使用的时候再打开
//	@Bean
//	public  AccessFilter accessFilter(){
//	   return new AccessFilter();
//    }
    //对于异常的过滤
    @Bean
    public ErrorFilter errorFilter(){
	    return new ErrorFilter();
    }
 }
