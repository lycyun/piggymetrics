**项目说明** 
###一种管理个人财务状况的简单方法  
  这是一个个人账务的开源应用，通过使用Spring Boot, Spring Cloud 和 Docker，
  用简洁的用户界面对微服务体系模式（Microservice Architecture Pattern）进行论证和呈现。
  现在对其添加中文注释，帮助大家理解
<br> 
<br> 
####项目结构** 
研究这几个的相互依赖
```
├─account-service     ------------------[ 账务服务（微服务1）
│     ├─client
│     ├─controller
│     ├─domain
│     ├─repository
│     └─service
│     └─security
├─notification-service ------------------[通知服务（微服务2）
│     ├─client
│     ├─controller
│     ├─domain
│     ├─repository
│     └─converter
│     └─service
│─statistics-service------------------[统计服务（微服务3）
│       ├─client
│       ├─controller
│       ├─domain
│       └─timeseries
│       ├─repository
│       └─converter
│       └─service
├─registry-----------------------------------[Eureka注册中心，服务发现=============配置
│
│
│
├─auth-service-----------------------------------[用户登录auth，都是与用户相关
│      ├─controller
│      ├─domain
│      ├─repository
│      └─service
│      └─security
├─config------------------------------------[配置中心=======================配置 ，加上yml文件，一会再详细研究
│      └─piggymetrics
│      └─config
├─gateway-----------------------------------[--√--zuul APi网关，也就是入口 =======配置加静态页
│   ├─css
│   ├─fonts
│   ├─museo-100
│   ├─museo-300
│   └─museo-500
│   ├─images
├─mongodb -----------------------------------[芒果db数据库的配置
│  └─dump
├─monitoring-----------------------------------[系统分析，检测 ========== == 配置
│ └─monitoring
```
<br> 


**技术选型：** 
- 核心框架：Spring cloud
- 视图框架：Spring boot
- 持久层框架：jpa
- 数据库：芒果
- 日志管理： logback
- 页面交互： jqurey
<br> 


 **本地部署**
- 记住，你要启动8个Spring Boot应用程序，4个MongoDB实例和RabbitMq。
-确保您的机器上有4 Gb RAM可用。您可以始终运行重要的服务：网关，注册，配置，认证服务和帐户服务。
-在你开始之前  
  安装 Docker and Docker Compose.
  导出环境变量: CONFIG_SERVICE_PASSWORD, NOTIFICATION_SERVICE_PASSWORD, STATISTICS_SERVICE_PASSWORD,
   ACCOUNT_SERVICE_PASSWORD, MONGODB_PASSWORD
-生产模式  
  在这种模式下，所有最新的镜像将从Docker Hub中提取。只需复制 docker-compose.yml并且执行 docker-compose up -d。?
-开发模式  
  如果你想自己构建镜像（例如在代码中有一些变化），建议你使用maven克隆所有的库和artifacts?。.
  然后，继承docker-compose.yml运行 docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d docker-compose.dev.yml 
  可以在本地构建映像，并开放所有容器端口以方便开发。
-重要节点  
  http://DOCKER-HOST:80 - Gateway
  http://DOCKER-HOST:8761 - Eureka Dashboard
  http://DOCKER-HOST:9000/hystrix - Hystrix Dashboard
  http://DOCKER-HOST:8989 - Turbine stream (source for the Hystrix Dashboard)
  http://DOCKER-HOST:15672 - RabbitMq 管理 (default login/password: guest/guest)
-说明  
  所有Spring Boot应用程序都需要运行Config Server进行启动。因为使用了fail-fast Spring Boot属性和restart: always?docker-compose?选项，
  我们可以同时启动所有容器。 这意味着在Config Server启动并运行之前，所有依赖的容器将尝试重新启动。
  此外，服务发现机制需要在所有应用程序启动后一段时间才能生效。
  在实例，Eureka服务器和客户端都在其本地缓存中具有相同的元数据前，服务发现机制将不可用。因此，它可能需要3次心跳。默认心跳周期为30秒
