package com.piggymetrics.account.service;
import com.piggymetrics.account.domain.Account;
import com.piggymetrics.account.domain.User;
public interface AccountService {
	/**
	 * 寻找账户，通过用户名
	 *
	 * @param accountName
	 * @return found account
	 */
	Account findByName(String accountName);

	/**
     * 创建记账
	 *
	 * @param user
	 * @return created account
	 */
	Account create(User user);

	/**
	 * 更改账务
	 * @param name
	 * @param update
	 */
	void saveChanges(String name, Account update);
}
