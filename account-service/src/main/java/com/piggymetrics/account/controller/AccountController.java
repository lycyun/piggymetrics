package com.piggymetrics.account.controller;

import com.piggymetrics.account.domain.Account;
import com.piggymetrics.account.domain.User;
import com.piggymetrics.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.security.Principal;
@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

    /**
     *  获取演示账户数据（例如预先输入的收益/开支明细）
     * @param name
     * @return
     */
	@PreAuthorize("#oauth2.hasScope('server') or #name.equals('demo')")
	@RequestMapping(path = "/{name}",method = RequestMethod.GET)
	public Account getAccountByName(@PathVariable String name) {
		return accountService.findByName(name);
	}

	/**
	 * 获取当前账户数据
	 * @param principal
	 * @return
	 */
	@RequestMapping(path = "/current",method = RequestMethod.GET)
	public Account getCurrentAccount(Principal principal) {
		return accountService.findByName(principal.getName());
	}

    /**
     *  保存当前账户数据
     * @param principal ，这是安全里面的内容
     * @param account
     */
	@RequestMapping(path = "/current",method = RequestMethod.PUT)
	public void saveCurrentAccount(Principal principal, @Valid @RequestBody Account account) {
		accountService.saveChanges(principal.getName(), account);
	}

    /**
     * 注册新账户
     * @param user
     * @return
     */
	@RequestMapping(path = "/",method = RequestMethod.POST)
	public Account createNewAccount(@Valid @RequestBody User user) {
	    //Valid对于接收到的数据有一个限制

		return accountService.create(user);
	}
}
