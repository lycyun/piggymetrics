package com.piggymetrics.account.repository;

import com.piggymetrics.account.domain.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *  dao层
 *
 * CrudRepository继承这个，这是使用了jpa的原理
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, String> {

	Account findByName(String name);

}
